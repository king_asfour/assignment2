/**
 * utilitiess.js - useful functions for the webpage
 * @author Samir Abo-Assfour
 * @version 1.0.0
 */
'use strict'

/**
 * initialize the options of the base select & to select
 */
function initialize_Select_Options_Data() {
    let fromCurr = document.querySelector('#base_currency_select');
    let toCurr = document.querySelector('#to_currency_select')
    fetch("../JSON/currency.json")
        .then((resp) => {
            return resp.json();
        })
        .then((data) => {
            for (let d of Object.values(data)) {
                let options1 = document.createElement('option');
                let options2 = document.createElement('option');
                options1.append(d.name);
                options2.append(d.name);
                options1.setAttribute('value', d.code);
                options2.setAttribute('value', d.code);
                toCurr.append(options1);
                fromCurr.append(options2);
            }
        })
    fetch("../JSON/currency.json").catch((err) => { console.log("Error during the fetch. Try again", err) });
}

/**
 * Creates the link to access the api with specified values from the user's selection 
 * @returns the link with the from/to/amount
 */
function setValueOfUrl() {
    let from = document.querySelector('#base_currency_select').value;
    let to = document.querySelector('#to_currency_select').value;
    let amount = document.querySelector("#amount").value;

    let fromLink = urlObj.fromQry + from;
    let toLink = urlObj.toQry + to;
    let amountLink = urlObj.amountQry + amount;

    let absoluteURL = getAbsoluteRequestURL(fromLink, toLink, amountLink);
    return absoluteURL;
}

/**
 * take a value and round it with 2 number after the dot (ex: 34.3245235 --> 34.32)
 * @param {*} conversion 
 * @returns value round up at the hundreth
 */
function roundUp(conversion) {
    const hundredth = 100;
    return Math.round(conversion * hundredth) / hundredth
}

/**
 * Creates the table's header
 * @param {*} objData 
 */
function createTableSkeleton(objData) {
    let table = document.querySelector('#project-table');
    let thead = document.createElement('thead');
    let tr = document.createElement('tr');

    for (let tHeader of Object.keys(objData)) {
        let th = document.createElement('th');
        th.append(tHeader);
        tr.append(th);
    }
    thead.append(tr);
    table.append(thead);
}

/**
 * Creates object in a specific order so that it can fetch
 * the appropriate value of the api in the order requested.
 * @param {*} from 
 * @param {*} to 
 * @param {*} rate 
 * @param {*} amount 
 * @param {*} payment 
 * @param {*} date 
 * @returns object with api's value in the respected order
 */
function createObject(from, to, rate, amount, payment, date) {
    let dateTyp = new Date;
    let newDate = date + ' - ' + dateTyp.getHours() + ':' + dateTyp.getMinutes() + ':' + dateTyp.getSeconds();
    let roundUpPayment = roundUp(payment);

    let objQuery = {
        "from": from,
        "to": to,
        "rate": rate,
        "amount": amount,
        "payment": roundUpPayment,
        "date": newDate
    };
    return objQuery;
}
/**
 * Sets if a button is enabled or disabled.
 * @param {*} buttonID 
 * @param {*} enabled 
 */
function setButtonEnabled(buttonID, enabled) {
    let button = document.querySelector(`button#${buttonID}`);
    button.disabled = enabled;
}
/**
 * Takes in an object of booleans and returns true if all of them are true.
 * @param {*} verifyValues 
 * @returns 
 */
function allValid(verifyValues) {
    let valid = true;
    for (let k in verifyValues) {
        if (!verifyValues[k]) {
            valid = false;
        }
    }
    return valid;
}
/**
 * Looks at the input given and if it has the default value, changes the valid variable to false
 * @param {*} userInput 
 * @returns return true if the input has a different value then the default one. 
 *           false if it is a default value
 */
function verifyNull(userInput) {
    let valid = true;
    if (userInput.value == "-") {
        valid = false;
    }
    if (userInput.value == '') {
        valid = false
    }
    return valid;

}