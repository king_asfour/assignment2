/**
 * exchangeRate.js - main logic for the webpage
 * @author Samir Abo-Assfour
 * @version 1.0.0
 */

'use strict'
//Applies the DOMContentLoaded on the init functions created below
document.addEventListener("DOMContentLoaded", init);

//Help to the creation of the url to access the API
const urlObj = {
    mainUrl: `https://api.exchangerate.host/convert`,
    fromQry: `?from=`,
    toQry: `&to=`,
    amountQry: `&amount=`,
};
//When all true, all value were set accordingly and can be processed
let verifyValues = { base: false, to: false, amount: false };

/**
 * Functions that creates the link to access and fetch the values from the restful API
 * @param {*} from 
 * @param {*} to 
 * @param {*} amount 
 * @returns the full link of the api web page but, without the specification of from/to/amount
 */
function getAbsoluteRequestURL(from, to, amount) {
    let absoluteLink = urlObj.mainUrl + from + to + amount;
    return absoluteLink;
}

/**
 * Request and fetch data from the URL created dynamically. 
 * When all data is fetched, display the table using 
 * createTableSkeleton() and displayResult()
 */
function getData() {
    let absoluteLink = setValueOfUrl();

    fetch(absoluteLink)
        .then((resp) => {
            return resp.json();
        })
        .then((data) => {
            let obj = createObject(
                data.query.from,
                data.query.to,
                data.info.rate,
                data.query.amount,
                data.result,
                data.date
            )
            if (document.querySelector('table').tHead == null) {
                createTableSkeleton(obj);
            }
            displayResult2(obj);
        })
    fetch(absoluteLink).catch((err => { console.log("Error during fetch of the api. Try again", err) }));
}
/**
 * display the result of the conversion and some extra information like the date & time
 * @param {*} obj 
 */
function displayResult2(obj) {
    let table = document.querySelector('#project-table');
    let tbody = document.querySelector('#tablegen');
    let tr = document.createElement('tr');

    for (let [a, b] of Object.entries(obj)) {
        let td = document.createElement('td');
        td.append(b);
        tr.append(td);
    }
    tbody.append(tr);
    table.append(tbody);
}

/**
 * initialize global variables and set even listeners
 * @param {*} event 
 */
function init(event) {
    //Global variables
    let button = document.getElementById('get_data_btn');
    let base = document.querySelector('#base_currency_select');
    let to = document.querySelector('#to_currency_select');
    let amount = document.querySelector('#amount');

    //NOTE: Object necessary to check if all the user inputs 
    //are correct before enabling the convert button
    let objUser = { "base": base, "to": to, "amount": amount };

    initialize_Select_Options_Data();


    for (let k in objUser) {
        objUser[k].addEventListener('blur', function () {
            verifyValues[k] = verifyNull(objUser[k]);
            setButtonEnabled(button.id, !allValid(verifyValues));
        })
    }

    button.addEventListener('click', getData);
}